package com.csnth.ketoan.report.dto.form;

public class SAProviderSearchForm {

	private String transactionName;

	public String getTransactionName() {
		return transactionName;
	}

	public void setTransactionName(String transactionName) {
		this.transactionName = transactionName;
	}

}
