package com.csnth.ketoan.report.dto.dxo;

public class SAProviderSearchViewDxoDto {

	private String transactionName;

	public String getTransactionName() {
		return transactionName;
	}

	public void setTransactionName(String transactionName) {
		this.transactionName = transactionName;
	}

}
