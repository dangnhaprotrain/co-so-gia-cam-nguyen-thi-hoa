package com.csnth.ketoan.admin.service.impl;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.csnth.ketoan.admin.dto.dxo.FindAllTransactionDxoDto;
import com.csnth.ketoan.admin.dto.dxo.TransactionTypeEditDxoDto;
import com.csnth.ketoan.admin.dto.rst.UpdateTransactionTypeRstDto;
import com.csnth.ketoan.admin.entities.mapper.FindAllTransactionTypeMapper;
import com.csnth.ketoan.admin.repository.IFindAllTransactionTypeRepository;
import com.csnth.ketoan.admin.service.ITransactionTypeService;
import com.csnth.ketoan.common.dto.DXO;
import com.csnth.ketoan.common.dto.RST;
import com.csnth.ketoan.common.entities.generate.TransactionType;
import com.csnth.ketoan.common.enums.EDeleteFlg;
import com.csnth.ketoan.common.enums.ETableAction;
import com.csnth.ketoan.common.repository.ITransactionTypeRepository;
import com.csnth.ketoan.common.util.BeanUtil;
import com.csnth.ketoan.common.util.DateUtil;

@Service
public class TransactionTypeServiceImpl implements ITransactionTypeService {

	@Autowired
	private ITransactionTypeRepository transactionTypeRepository;
	
	@Autowired
	private IFindAllTransactionTypeRepository findAllTransactionTypeRepository;

	@Override
	public Page<TransactionType> findAll(Pageable pageable, DXO dxoDto) {
		FindAllTransactionDxoDto dxo = (FindAllTransactionDxoDto) dxoDto;
		String transactionNameSearch =  "%" + StringUtils.trim(StringUtils.defaultIfEmpty(dxo.getTransactionName(), StringUtils.EMPTY)) +"%";
		Page<TransactionType> pageData = transactionTypeRepository
												.findByTransactionNameLikeIgnoreCaseAndDeleteFlg(transactionNameSearch
																								, EDeleteFlg.ACTIVE.getCode()
																								, pageable);
		return pageData;
	}

	@Override
	public RST updateTransactionType(DXO dxoDto) {
		TransactionTypeEditDxoDto dxo = (TransactionTypeEditDxoDto) dxoDto;
		TransactionType entityPrm = null;
		if(ETableAction.DELETE.getLabel().equals(dxo.getAction()) && dxo.getId() != null && dxo.getId() != 0){
			entityPrm = transactionTypeRepository.findOne(dxo.getId());
			entityPrm.setDeleteFlg(EDeleteFlg.DELETED.getCode());
		}
		else if(ETableAction.RESTORE.getLabel().equals(dxo.getAction()) && dxo.getId() != null && dxo.getId() == 0){
			entityPrm = transactionTypeRepository.findOne(dxo.getId());
			entityPrm.setDeleteFlg(EDeleteFlg.ACTIVE.getCode());
		}
		else {
			if (dxo.getId() == null || dxo.getId() == 0) {
				entityPrm = (TransactionType) BeanUtil.createAndCopyPropertiesNative(dxo, TransactionType.class);
				entityPrm.setDeleteFlg(EDeleteFlg.ACTIVE.getCode());
				entityPrm.setCreatedDate(DateUtil.getCurrentTime());
				entityPrm.setCreatedBy(dxo.getUpdatedBy());
			}else{
				entityPrm = transactionTypeRepository.findOne(dxo.getId());
				BeanUtil.copyPropertiesNative(dxo, entityPrm);
			}
		}
		entityPrm.setUpdatedDate(DateUtil.getCurrentTime());
		TransactionType resultUpdate = transactionTypeRepository.saveAndFlush(entityPrm);

		UpdateTransactionTypeRstDto rst = (UpdateTransactionTypeRstDto) BeanUtil.createAndCopyPropertiesNative(dxo
																										, UpdateTransactionTypeRstDto.class);
		BeanUtil.copyPropertiesNative(resultUpdate, rst);
		return rst;
	}

	@Override
	public Page<FindAllTransactionTypeMapper> findAll(DXO dxoDto, Pageable pageable) {
		FindAllTransactionDxoDto dxo = (FindAllTransactionDxoDto) dxoDto;
		String transactionNameSearch =  "%" + StringUtils.trim(StringUtils.defaultIfEmpty(dxo.getTransactionName(), StringUtils.EMPTY)) +"%";
		Page<FindAllTransactionTypeMapper> pageData = findAllTransactionTypeRepository
																		.findAll(transactionNameSearch
																		, EDeleteFlg.ACTIVE.getCode()
																		, pageable);
		return pageData;
	}

}
