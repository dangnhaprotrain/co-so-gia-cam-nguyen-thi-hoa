package com.csnth.ketoan.admin.service.impl;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.csnth.ketoan.admin.dto.dxo.FindAllCompanyDxoDto;
import com.csnth.ketoan.admin.entities.mapper.FindAllCompanyMapper;
import com.csnth.ketoan.admin.repository.ICompanyRepository;
import com.csnth.ketoan.admin.service.ICompanyService;
import com.csnth.ketoan.common.dto.DXO;

@Service
public class CompanyServiceImpl implements ICompanyService {

	@Autowired
	private ICompanyRepository companyRepository;

	public Page<FindAllCompanyMapper> findAllCompany(Pageable pageable, DXO dxoDto) {
		FindAllCompanyDxoDto dxo = (FindAllCompanyDxoDto) dxoDto;
		String companyNameSearch = StringUtils.trim(StringUtils.defaultIfEmpty(dxo.getCompanyName(), StringUtils.EMPTY)).toLowerCase();
		Page<FindAllCompanyMapper> pageData = companyRepository
												.findAllCompany("%".concat(companyNameSearch).concat("%")
																, dxo.getDeletedFlg()
																, pageable);
		return pageData;
	}

}
