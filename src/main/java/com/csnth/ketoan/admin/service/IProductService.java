package com.csnth.ketoan.admin.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.csnth.ketoan.common.dto.DXO;
import com.csnth.ketoan.common.dto.RST;
import com.csnth.ketoan.common.entities.generate.Product;
import com.csnth.ketoan.common.service.BaseService;

public interface IProductService extends BaseService {
	Page<Product> findAllByPaging(Pageable pageable, DXO dxoDto);

	List<Product> findAll();

	RST updateProduct(DXO dxoDto);
}
