package com.csnth.ketoan.admin.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.csnth.ketoan.admin.entities.mapper.FindAllTransactionTypeMapper;
import com.csnth.ketoan.common.dto.DXO;
import com.csnth.ketoan.common.dto.RST;
import com.csnth.ketoan.common.entities.generate.TransactionType;
import com.csnth.ketoan.common.service.BaseService;

public interface ITransactionTypeService extends BaseService {
	Page<TransactionType> findAll(Pageable pageable, DXO dxoDto);

	RST updateTransactionType(DXO dxoDto);

	Page<FindAllTransactionTypeMapper> findAll(DXO dxoDto, Pageable pageable);
}
