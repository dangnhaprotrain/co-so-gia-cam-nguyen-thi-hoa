package com.csnth.ketoan.admin.service;

import com.csnth.ketoan.admin.entities.mapper.FindUserByRoleCompanyMapper;
import com.csnth.ketoan.common.dto.DXO;
import com.csnth.ketoan.common.service.BaseService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface IUserByRoleCompanyService extends BaseService {
	Page<FindUserByRoleCompanyMapper> findAllUser(Pageable pageable, DXO dxoDto);
}
