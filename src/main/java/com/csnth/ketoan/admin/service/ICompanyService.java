package com.csnth.ketoan.admin.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.csnth.ketoan.admin.entities.mapper.FindAllCompanyMapper;
import com.csnth.ketoan.common.dto.DXO;
import com.csnth.ketoan.common.service.BaseService;

public interface ICompanyService extends BaseService {
	Page<FindAllCompanyMapper> findAllCompany(Pageable pageable, DXO dxoDto);
}
