package com.csnth.ketoan.admin.service.impl;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.csnth.ketoan.admin.dto.dxo.FindAllProductDxoDto;
import com.csnth.ketoan.admin.dto.dxo.ProductEditDxoDto;
import com.csnth.ketoan.admin.dto.rst.UpdateProductRstDto;
import com.csnth.ketoan.admin.service.IProductService;
import com.csnth.ketoan.common.dto.DXO;
import com.csnth.ketoan.common.dto.RST;
import com.csnth.ketoan.common.entities.generate.Product;
import com.csnth.ketoan.common.enums.EDeleteFlg;
import com.csnth.ketoan.common.repository.IProductRepository;
import com.csnth.ketoan.common.util.BeanUtil;

@Service
public class ProductServiceImpl implements IProductService {

	@Autowired
	private IProductRepository productRepository;

	public Page<Product> findAllByPaging(Pageable pageable, DXO dxoDto) {
		FindAllProductDxoDto dxo = (FindAllProductDxoDto) dxoDto;
		String productNameSearch = StringUtils.trim(StringUtils.defaultIfEmpty(dxo.getProductName()
																			, StringUtils.EMPTY)
													).toLowerCase();
		if (dxo.getDeleteFlg() == null || dxo.getDeleteFlg() == 0) {
			return productRepository
						.findByProductNameLikeIgnoreCase("%".concat(productNameSearch).concat("%"), pageable);
		}
		return productRepository
				.findByProductNameLikeIgnoreCaseAndDeleteFlg("%".concat(productNameSearch).concat("%")
															, dxo.getDeleteFlg()
															, pageable);
	}

	@Override
	public RST updateProduct(DXO dxoDto) {
		ProductEditDxoDto dxo = (ProductEditDxoDto) dxoDto;
		Product entityPrm = (Product) BeanUtil.createAndCopyPropertiesNative(dxo, Product.class);
		if (dxo.getId() == null || dxo.getId() == 0) {
			entityPrm.setDeleteFlg(EDeleteFlg.ACTIVE.getCode());
		}
		Product resultUpdate = productRepository.saveAndFlush(entityPrm);

		UpdateProductRstDto rst = (UpdateProductRstDto) BeanUtil.createAndCopyPropertiesNative(resultUpdate
																							, UpdateProductRstDto.class);
		rst.setAction(dxo.getAction());
		return rst;
	}

	@Override
	public List<Product> findAll() {
		return productRepository.findByDeleteFlg(EDeleteFlg.ACTIVE.getCode());
	}

}
