package com.csnth.ketoan.admin.service.impl;

import com.csnth.ketoan.admin.dto.dxo.FindAllUserDxoDto;
import com.csnth.ketoan.admin.entities.mapper.FindUserByRoleCompanyMapper;
import com.csnth.ketoan.admin.repository.IUserByRoleCompanyRepository;
import com.csnth.ketoan.admin.service.IUserByRoleCompanyService;
import com.csnth.ketoan.common.dto.DXO;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class UserByRoleCompanyServiceImpl implements IUserByRoleCompanyService {

	@Autowired
	private IUserByRoleCompanyRepository userByRoleCompanyRepository;

	public Page<FindUserByRoleCompanyMapper> findAllUser(Pageable pageable, DXO dxoDto) {
		FindAllUserDxoDto dxo = (FindAllUserDxoDto) dxoDto;
		String fullNameSearch = StringUtils.trim(StringUtils.defaultIfEmpty(dxo.getFullName(), StringUtils.EMPTY)).toLowerCase();
		String companyNameSearch = StringUtils.trim(StringUtils.defaultIfEmpty(dxo.getCompanyName(), StringUtils.EMPTY)).toLowerCase();
		Page<FindUserByRoleCompanyMapper> pageData = userByRoleCompanyRepository
												.findAllUser("%".concat(fullNameSearch).concat("%")
																, dxo.getStatus()
																, "%".concat(companyNameSearch).concat("%")
																, dxo.getDeletedFlg()
																, pageable);
		return pageData;
	}

}
