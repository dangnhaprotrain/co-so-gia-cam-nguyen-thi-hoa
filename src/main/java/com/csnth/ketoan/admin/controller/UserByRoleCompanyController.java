package com.csnth.ketoan.admin.controller;

import com.csnth.ketoan.admin.dto.dxo.FindAllUserDxoDto;
import com.csnth.ketoan.admin.dto.form.UserViewSearchForm;
import com.csnth.ketoan.admin.service.IUserByRoleCompanyService;
import com.csnth.ketoan.common.constant.Router;
import com.csnth.ketoan.common.controller.impl.BaseControllerImpl;
import com.csnth.ketoan.common.util.BeanUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.data.web.SortDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;

@Controller
public class UserByRoleCompanyController extends BaseControllerImpl {

	@Autowired
	private IUserByRoleCompanyService userByRoleCompanyService;

	@GetMapping(value = Router.ADMIN.URI.USER)
	public String view(@ModelAttribute("searchForm") UserViewSearchForm searchForm
						, Model model
						, @PageableDefault(size = DEFAULT_PAGE_SIZE) 
						@SortDefault.SortDefaults({@SortDefault(sort = "id", direction = Sort.Direction.ASC) }) 
						Pageable pageable) {
		FindAllUserDxoDto dxo = (FindAllUserDxoDto) BeanUtil
															.createAndCopyPropertiesNative(searchForm
																						, FindAllUserDxoDto.class);
		model.addAttribute("pageList", userByRoleCompanyService.findAllUser(pageable, dxo));
		return Router.ADMIN.PAGE.USER;
	}
}
