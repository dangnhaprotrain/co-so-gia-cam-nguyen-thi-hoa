package com.csnth.ketoan.admin.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.data.web.SortDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;

import com.csnth.ketoan.admin.dto.dxo.FindAllCompanyDxoDto;
import com.csnth.ketoan.admin.dto.form.CompanyViewSearchForm;
import com.csnth.ketoan.admin.service.ICompanyService;
import com.csnth.ketoan.common.constant.Router;
import com.csnth.ketoan.common.controller.impl.BaseControllerImpl;
import com.csnth.ketoan.common.util.BeanUtil;

@Controller
public class CompanyController extends BaseControllerImpl {

	@Autowired
	private ICompanyService companyService;

	@GetMapping(value = Router.ADMIN.URI.COMPANY)
	public String view(@ModelAttribute("searchForm") CompanyViewSearchForm searchForm
						, Model model
						, @PageableDefault(size = DEFAULT_PAGE_SIZE) 
						@SortDefault.SortDefaults({@SortDefault(sort = "id", direction = Sort.Direction.ASC) }) 
						Pageable pageable) {
		FindAllCompanyDxoDto dxo = (FindAllCompanyDxoDto) BeanUtil
															.createAndCopyPropertiesNative(searchForm
																						, FindAllCompanyDxoDto.class);
		model.addAttribute("pageList", companyService.findAllCompany(pageable, dxo));
		return Router.ADMIN.PAGE.COMPANY;
	}
}
