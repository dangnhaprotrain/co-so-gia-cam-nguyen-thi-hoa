package com.csnth.ketoan.admin.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.data.web.SortDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;

import com.csnth.ketoan.admin.dto.dxo.FindAllTransactionTypePriceDxoDto;
import com.csnth.ketoan.admin.dto.form.TransactionTypePriceSearchForm;
import com.csnth.ketoan.admin.service.ITransactionTypeService;
import com.csnth.ketoan.common.constant.Router;
import com.csnth.ketoan.common.controller.impl.BaseControllerImpl;
import com.csnth.ketoan.common.entities.generate.TransactionType;
import com.csnth.ketoan.common.util.BeanUtil;

@Controller
public class TransactionTypePriceController extends BaseControllerImpl {
	@Autowired
	private ITransactionTypeService transactionService;

	@GetMapping(value = Router.ADMIN.URI.TRANSACTION_TYPE_PRICE)
	public String view(@ModelAttribute("searchForm") TransactionTypePriceSearchForm searchForm
					, Model model
					, @PageableDefault(size = DEFAULT_PAGE_SIZE) @SortDefault.SortDefaults({
					  @SortDefault(sort = "id", direction = Sort.Direction.ASC) }) 
					  Pageable pageable) {
		FindAllTransactionTypePriceDxoDto dxo = (FindAllTransactionTypePriceDxoDto) BeanUtil
																						.createAndCopyPropertiesNative(searchForm
																														, FindAllTransactionTypePriceDxoDto.class);
		Page<TransactionType> rst = transactionService.findAll(pageable, dxo);
		model.addAttribute("pageList", rst);
		return Router.ADMIN.PAGE.TRANSACTION_TYPE_PRICE;
	}
}
