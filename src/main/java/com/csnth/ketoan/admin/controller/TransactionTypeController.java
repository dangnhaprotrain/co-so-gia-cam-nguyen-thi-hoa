package com.csnth.ketoan.admin.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.data.web.SortDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.csnth.ketoan.admin.dto.dxo.FindAllTransactionDxoDto;
import com.csnth.ketoan.admin.dto.dxo.TransactionTypeEditDxoDto;
import com.csnth.ketoan.admin.dto.form.TransactionTypeEditForm;
import com.csnth.ketoan.admin.dto.form.TransactionTypeSearchForm;
import com.csnth.ketoan.admin.dto.rst.UpdateTransactionTypeRstDto;
import com.csnth.ketoan.admin.entities.mapper.FindAllTransactionTypeMapper;
import com.csnth.ketoan.admin.service.IProductService;
import com.csnth.ketoan.admin.service.ITransactionTypeService;
import com.csnth.ketoan.common.constant.Router;
import com.csnth.ketoan.common.controller.impl.BaseControllerImpl;
import com.csnth.ketoan.common.dto.ErrorInfo;
import com.csnth.ketoan.common.entities.generate.Product;
import com.csnth.ketoan.common.enums.ETableAction;
import com.csnth.ketoan.common.util.BeanUtil;
import com.google.gson.Gson;

@Controller
public class TransactionTypeController extends BaseControllerImpl {
	@Autowired
	private ITransactionTypeService transactionService;

	@Autowired
	private IProductService productService;

	@GetMapping(value = Router.ADMIN.URI.TRANSACTION_TYPE)
	public String view(@ModelAttribute("searchForm") TransactionTypeSearchForm searchForm, Model model,
					@PageableDefault(size = DEFAULT_PAGE_SIZE) @SortDefault.SortDefaults({
					@SortDefault(sort = "id", direction = Sort.Direction.ASC) }) Pageable pageable) {
		FindAllTransactionDxoDto dxo = (FindAllTransactionDxoDto) BeanUtil.createAndCopyPropertiesNative(searchForm,
				FindAllTransactionDxoDto.class);
		Page<FindAllTransactionTypeMapper> rst = transactionService.findAll(dxo, pageable);
		model.addAttribute("pageList", rst);
		model.addAttribute("products", convertToDataList(productService.findAll()));
		return Router.ADMIN.PAGE.TRANSACTION_TYPE;
	}

	@PostMapping(value = Router.ADMIN.URI.TRANSACTION_TYPE)
	@ResponseBody
	public String update(@Validated TransactionTypeEditForm form
						, BindingResult result
						, Model model
						, HttpServletRequest request
						) {
		if(!ETableAction.DELETE.getLabel().equals(form.getAction()) 
				&& !ETableAction.RESTORE.getLabel().equals(form.getAction())){
			if (result.hasErrors()) {
				List<FieldError> errs = result.getFieldErrors();
				for (FieldError fieldError : errs) {
					form.getErrInfo().add(new ErrorInfo(fieldError.getDefaultMessage(), fieldError.getField()));
				}
				return (new Gson()).toJson(form);
			}
		}else{
			if(form.getId() == null || form.getId() == 0){
				return (new Gson()).toJson(form);
			}
		}
		TransactionTypeEditDxoDto dxo = (TransactionTypeEditDxoDto) BeanUtil
																		.createAndCopyPropertiesNative(form
																									, TransactionTypeEditDxoDto.class);
		dxo.setUpdatedBy(getUserLoginInfor().getUserId());
		UpdateTransactionTypeRstDto rst = (UpdateTransactionTypeRstDto) transactionService.updateTransactionType(dxo);
		return (new Gson()).toJson(rst);
	}

	private String convertToDataList(List<Product> products){
		Map<String,String> dataList = new HashMap<>();
		if(CollectionUtils.isNotEmpty(products)){
			for (Product product : products) {
				dataList.put(product.getId().toString(), product.getProductName());
			}
		}
		return (new Gson()).toJson(dataList);
	}
}
