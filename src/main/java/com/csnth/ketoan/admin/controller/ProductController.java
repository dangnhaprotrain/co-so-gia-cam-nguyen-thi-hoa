package com.csnth.ketoan.admin.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.data.web.SortDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.csnth.ketoan.admin.dto.dxo.FindAllProductDxoDto;
import com.csnth.ketoan.admin.dto.dxo.ProductEditDxoDto;
import com.csnth.ketoan.admin.dto.form.ProductEditForm;
import com.csnth.ketoan.admin.dto.form.ProductSearchForm;
import com.csnth.ketoan.admin.dto.rst.UpdateProductRstDto;
import com.csnth.ketoan.admin.service.IProductService;
import com.csnth.ketoan.common.constant.Router;
import com.csnth.ketoan.common.controller.impl.BaseControllerImpl;
import com.csnth.ketoan.common.dto.ErrorInfo;
import com.csnth.ketoan.common.util.BeanUtil;
import com.google.gson.Gson;

@Controller
public class ProductController extends BaseControllerImpl {

	@Autowired
	private IProductService productService;

	@GetMapping(value = Router.ADMIN.URI.PRODUCT)
	public String view(@ModelAttribute("searchForm") ProductSearchForm searchForm
						, Model model
						, @PageableDefault(size = DEFAULT_PAGE_SIZE) 
						@SortDefault.SortDefaults({@SortDefault(sort = "productName", direction = Sort.Direction.ASC) }) 
						Pageable pageable) {
		FindAllProductDxoDto dxo = (FindAllProductDxoDto) BeanUtil
															.createAndCopyPropertiesNative(searchForm
																						, FindAllProductDxoDto.class);
		model.addAttribute("pageList", productService.findAllByPaging(pageable, dxo));
		return Router.ADMIN.PAGE.PRODUCT;
	}
	
	@PostMapping(value = Router.ADMIN.URI.PRODUCT)
	@ResponseBody
	public String update(@Validated ProductEditForm form
						, BindingResult result
						, Model model
						, HttpServletRequest request
						) {
		if (result.hasErrors()) {
			List<FieldError> errs = result.getFieldErrors();
			for (FieldError fieldError : errs) {
				form.getErrInfo().add(new ErrorInfo(fieldError.getDefaultMessage(), fieldError.getField()));
			}
			return (new Gson()).toJson(form);
		}
		ProductEditDxoDto dxo = (ProductEditDxoDto) BeanUtil.createAndCopyPropertiesNative(form
																						, ProductEditDxoDto.class);
		return (new Gson()).toJson((UpdateProductRstDto)productService.updateProduct(dxo));
		
	}
}
