package com.csnth.ketoan.admin.entities.mapper;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class FindAllTransactionTypePriceMapper implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	private Long id;

	@Column(name = "transaction_type_id")
	private Long transactionTypeId;

	@Column(name = "company_id")
	private Long companyId;

	@Column(name = "price_default")
	private BigDecimal priceDefault;

	@Column(name = "time_apply_from")
	private Timestamp timeApplyFrom;

	@Column(name = "time_apply_to")
	private Timestamp timeApplyTo;

	private Short status;

	@Column(name = "transaction_name")
	private String transactionName;

	@Column(name = "company_name")
	private String companyName;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getTransactionTypeId() {
		return transactionTypeId;
	}

	public void setTransactionTypeId(Long transactionTypeId) {
		this.transactionTypeId = transactionTypeId;
	}

	public Long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}

	public BigDecimal getPriceDefault() {
		return priceDefault;
	}

	public void setPriceDefault(BigDecimal priceDefault) {
		this.priceDefault = priceDefault;
	}

	public Timestamp getTimeApplyFrom() {
		return timeApplyFrom;
	}

	public void setTimeApplyFrom(Timestamp timeApplyFrom) {
		this.timeApplyFrom = timeApplyFrom;
	}

	public Timestamp getTimeApplyTo() {
		return timeApplyTo;
	}

	public void setTimeApplyTo(Timestamp timeApplyTo) {
		this.timeApplyTo = timeApplyTo;
	}

	public Short getStatus() {
		return status;
	}

	public void setStatus(Short status) {
		this.status = status;
	}

	public String getTransactionName() {
		return transactionName;
	}

	public void setTransactionName(String transactionName) {
		this.transactionName = transactionName;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

}
