package com.csnth.ketoan.admin.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.data.repository.query.Param;

import com.csnth.ketoan.admin.entities.mapper.FindAllCompanyMapper;
import com.csnth.ketoan.admin.entities.mapper.FindAllTransactionTypePriceMapper;

public interface IFindAllTransactionTypePriceRepository extends Repository<FindAllTransactionTypePriceMapper, Long> {
	@Query(value = "SELECT"
					+ "	c1.ID AS company_id " 
					+ "	, c1.company_name "
					+ "	, c1.phone_number "
					+ "	, c1.delete_flg "
					+ "	, c1.branch_id "
					+ "	, c2.company_name parent_company_name "
					+ "FROM "
					+ "	company c1 "
					+ "LEFT JOIN company c2 ON c1.branch_id = c2.ID "
					+ "WHERE " 
					+ "	1 = 1 "
					+ "AND lower(c1.company_name) LIKE :company_name "
					+ "AND (CASE WHEN :delete_flg <> 0 THEN c1.delete_flg=:delete_flg ELSE TRUE END) "  
					+ " ORDER BY ?#{#pageable} "
			, countQuery = "SELECT "
							+ "	COUNT (1) "
							+ "FROM "
							+ "	company c1 " 
							+ "LEFT JOIN company c2 ON c1.branch_id = c2.ID "
							+ "WHERE " 
							+ "	1 = 1 "
							+ "AND lower(c1.company_name) LIKE :company_name "
							+ "AND (CASE WHEN :delete_flg <> 0 THEN c1.delete_flg=:delete_flg ELSE TRUE END) "  
							+ "ORDER BY ?#{#pageable} "
			, nativeQuery = true)
	Page<FindAllCompanyMapper> findAllCompany(@Param("company_name") String companyName
											, @Param("delete_flg") int deleteFlg
											, Pageable pageable);
}
