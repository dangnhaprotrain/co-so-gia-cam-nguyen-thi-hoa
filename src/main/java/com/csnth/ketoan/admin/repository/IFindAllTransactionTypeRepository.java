package com.csnth.ketoan.admin.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.data.repository.query.Param;

import com.csnth.ketoan.admin.entities.mapper.FindAllTransactionTypeMapper;

public interface IFindAllTransactionTypeRepository extends Repository<FindAllTransactionTypeMapper, Long> {
	@Query(value = "SELECT"
					+ " 	tt.id "
					+ " 	, tt.product_id "
					+ " 	, p.product_name "
					+ " 	, tt.transaction_name "
					+ " 	, tt.delete_flg "
					+ "FROM "
					+ "		transaction_type tt "
					+ "INNER JOIN product p ON tt.product_id = p.ID "
					+ "WHERE " 
					+ "		1 = 1 "
					+ "		AND lower(tt.transaction_name) LIKE :transaction_name "
					+ "AND (CASE WHEN :delete_flg <> 0 THEN tt.delete_flg=:delete_flg ELSE TRUE END) "  
					+ "AND (CASE WHEN :delete_flg <> 0 THEN p.delete_flg=:delete_flg ELSE TRUE END) " 
					+ "ORDER BY ?#{#pageable} "
			, countQuery = "SELECT "
					+ "	COUNT (1) "
					+ "FROM "
					+ "		transaction_type tt "
					+ "INNER JOIN product p ON tt.product_id = p.ID "
					+ "WHERE " 
					+ "		1 = 1 "
					+ "		AND lower(tt.transaction_name) LIKE :transaction_name "
					+ "AND (CASE WHEN :delete_flg <> 0 THEN tt.delete_flg=:delete_flg ELSE TRUE END) "  
					+ "AND (CASE WHEN :delete_flg <> 0 THEN p.delete_flg=:delete_flg ELSE TRUE END) " 
					+ "ORDER BY ?#{#pageable} "
			, nativeQuery = true)
	Page<FindAllTransactionTypeMapper> findAll(@Param("transaction_name") String transactionName
									, @Param("delete_flg") int deleteFlg
									, Pageable pageable);
}
