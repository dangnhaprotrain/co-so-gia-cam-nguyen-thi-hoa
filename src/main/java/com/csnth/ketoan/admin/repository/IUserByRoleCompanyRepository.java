package com.csnth.ketoan.admin.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.data.repository.query.Param;
import com.csnth.ketoan.admin.entities.mapper.FindUserByRoleCompanyMapper;

public interface IUserByRoleCompanyRepository extends Repository<FindUserByRoleCompanyMapper, Long> {
	@Query(value = "SELECT "
						+ "     u.id "
						+ "     , u.username "
						+ "     , u.fullname "
						+ "     , u.phone_number "
						+ "     , u.email "
						+ "     , u.status "
						+ " 	, r.id AS role_id "
						+ "		, r.role_name "
						+ "		, c.id AS company_id "
						+ "		, r.ID AS role_id "
						+ "		, r.role_name "
						+ "     , c.company_name "
						+ "     , c.phone_number AS company_phone_number "
						+ " FROM "
						+ "		users AS u "
						+ " INNER JOIN ROLE AS r "
						+ " ON u.role_id = r.id "
						+ " INNER JOIN company AS c "
						+ " ON u.company_id = c.id "
						+ "WHERE "
						+ "	1 = 1 "
						+ "AND lower(u.fullname) LIKE :full_name "
						+ "AND (CASE WHEN :status <> 0 THEN u.status=:status ELSE TRUE END) "
						+ "OR lower(c.company_name) LIKE :company_name "
						+ "AND (CASE WHEN :delete_flg <> 0 THEN c.delete_flg=:delete_flg ELSE TRUE END) "
						+ " ORDER BY ?#{#pageable} "
			, countQuery = "SELECT "
						+ "     count(1)"
						+ " FROM "
						+ "		users AS u "
						+ " INNER JOIN ROLE AS r "
						+ " ON u.role_id = r.id "
						+ " INNER JOIN company AS c "
						+ " ON u.company_id = c.id "
						+ " WHERE "
						+ "	1 = 1 "
						+ "AND lower(u.fullname) LIKE :full_name "
						+ "AND (CASE WHEN :status <> 0 THEN u.status=:status ELSE TRUE END) "
						+ "OR lower(c.company_name) LIKE :company_name "
						+ "AND (CASE WHEN :delete_flg <> 0 THEN c.delete_flg=:delete_flg ELSE TRUE END) "
			, nativeQuery = true)
	Page<FindUserByRoleCompanyMapper> findAllUser(@Param("full_name") String fullName,
												  @Param("status") int status,
												  @Param("company_name") String companyName,
												  @Param("delete_flg") int deleteFlg,
												  Pageable pageable);
}
