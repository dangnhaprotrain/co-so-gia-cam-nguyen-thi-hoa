package com.csnth.ketoan.admin.dto.form;

public class CompanyViewSearchForm {

	private String companyName;

	private String deletedFlg;

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getDeletedFlg() {
		return deletedFlg;
	}

	public void setDeletedFlg(String deletedFlg) {
		this.deletedFlg = deletedFlg;
	}
}
