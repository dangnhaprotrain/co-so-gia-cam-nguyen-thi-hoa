package com.csnth.ketoan.admin.dto.dxo;

import com.csnth.ketoan.common.dto.DXO;

public class FindAllUserDxoDto extends DXO {

	private static final long serialVersionUID = 1L;

	private String fullName;

	private int status;

	private String companyName;

	private int deletedFlg;

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public int getDeletedFlg() {
		return deletedFlg;
	}

	public void setDeletedFlg(int deletedFlg) {
		this.deletedFlg = deletedFlg;
	}

}
