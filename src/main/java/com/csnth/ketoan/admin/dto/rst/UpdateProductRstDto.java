package com.csnth.ketoan.admin.dto.rst;

import com.csnth.ketoan.common.dto.RST;

public class UpdateProductRstDto extends RST {
	private static final long serialVersionUID = 1L;

	private String action;

	private String productName;

	private Long id;

	private Short deleteFlg;

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Short getDeleteFlg() {
		return deleteFlg;
	}

	public void setDeleteFlg(Short deleteFlg) {
		this.deleteFlg = deleteFlg;
	}

}
