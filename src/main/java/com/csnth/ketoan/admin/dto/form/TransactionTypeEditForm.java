package com.csnth.ketoan.admin.dto.form;

import com.csnth.ketoan.common.dto.BaseForm;
import com.csnth.ketoan.common.validation.RequiredNotBlank;

public class TransactionTypeEditForm extends BaseForm {

	@RequiredNotBlank(name = "Transaction name")
	private String transactionName;
	private String action;
	private Long id;
	private Short index;
	@RequiredNotBlank(name = "Product name")
	private String productId;

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public Short getIndex() {
		return index;
	}

	public void setIndex(Short index) {
		this.index = index;
	}

	public String getTransactionName() {
		return transactionName;
	}

	public void setTransactionName(String transactionName) {
		this.transactionName = transactionName;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

}
