package com.csnth.ketoan.admin.dto.form;

public class TransactionTypePriceSearchForm {

	private String transactionName;

	public String getTransactionName() {
		return transactionName;
	}

	public void setTransactionName(String transactionName) {
		this.transactionName = transactionName;
	}

}
