package com.csnth.ketoan.admin.dto.dxo;

import com.csnth.ketoan.common.dto.DXO;

public class FindAllTransactionDxoDto extends DXO {

	private static final long serialVersionUID = 1L;

	private String transactionName;

	public String getTransactionName() {
		return transactionName;
	}

	public void setTransactionName(String transactionName) {
		this.transactionName = transactionName;
	}

}
