package com.csnth.ketoan.admin.dto.dxo;

import com.csnth.ketoan.common.dto.DXO;

public class FindAllProductDxoDto extends DXO {

	private static final long serialVersionUID = 1L;

	private String productName;

	private Short deleteFlg;

	public Short getDeleteFlg() {
		return deleteFlg;
	}

	public void setDeleteFlg(Short deleteFlg) {
		this.deleteFlg = deleteFlg;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

}
