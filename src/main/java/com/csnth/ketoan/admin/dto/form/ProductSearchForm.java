package com.csnth.ketoan.admin.dto.form;

public class ProductSearchForm {

	private String productName;

	private Short deleteFlg;

	public Short getDeleteFlg() {
		return deleteFlg;
	}

	public void setDeleteFlg(Short deleteFlg) {
		this.deleteFlg = deleteFlg;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

}
