package com.csnth.ketoan.admin.dto.rst;

import com.csnth.ketoan.common.dto.RST;

public class UpdateTransactionTypeRstDto extends RST {
	private static final long serialVersionUID = 1L;

	private String transactionName;
	private String action;
	private Long id;
	private Short index;
	private Long productId;

	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

	public Short getIndex() {
		return index;
	}

	public void setIndex(Short index) {
		this.index = index;
	}

	public String getTransactionName() {
		return transactionName;
	}

	public void setTransactionName(String transactionName) {
		this.transactionName = transactionName;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

}
