package com.csnth.ketoan.admin.dto.form;

import com.csnth.ketoan.common.dto.BaseForm;
import com.csnth.ketoan.common.validation.RequiredNotBlank;

public class ProductEditForm extends BaseForm {

	private String action;

	private Long id;

	@RequiredNotBlank(name = "Product name")
	private String productName;

	private Short deleteFlg;

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public Short getDeleteFlg() {
		return deleteFlg;
	}

	public void setDeleteFlg(Short deleteFlg) {
		this.deleteFlg = deleteFlg;
	}

}
