package com.csnth.ketoan.common.repository.impl;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import com.csnth.ketoan.common.entities.mapper.SequenceMapper;
import com.csnth.ketoan.common.repository.ISequenceRepository;

@Repository
public class SequenceRepositoryImpl implements ISequenceRepository {

	@PersistenceContext
	public EntityManager em;

	@Override
	public SequenceMapper getSeqNextVal(String seqName) {
		return (SequenceMapper) em.createNamedQuery("SeqSql").setParameter("seq_name", seqName).getSingleResult();
	}

}
