package com.csnth.ketoan.common.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.csnth.ketoan.common.entities.generate.Role;

public interface IRoleRepository extends JpaRepository<Role, Long> {
}
