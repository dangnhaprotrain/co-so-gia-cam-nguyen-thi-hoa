package com.csnth.ketoan.common.repository;

import com.csnth.ketoan.common.entities.mapper.SequenceMapper;

/**
 * User repository for CRUD operations.
 */
public interface ISequenceRepository {
	SequenceMapper getSeqNextVal(String seqName);

}
