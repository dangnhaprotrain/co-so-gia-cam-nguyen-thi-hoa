package com.csnth.ketoan.common.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.csnth.ketoan.common.entities.generate.User;

public interface IUserRepository extends JpaRepository<User, Long> {
	User findByUsername(String username);
}
