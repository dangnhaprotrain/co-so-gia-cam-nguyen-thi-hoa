package com.csnth.ketoan.common.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.csnth.ketoan.common.entities.generate.TransactionType;

public interface ITransactionTypeRepository extends JpaRepository<TransactionType, Long> {
	public Page<TransactionType> findByTransactionNameLikeIgnoreCase(String transactionName
																	, Pageable pageRequest);
	public Page<TransactionType> findByTransactionNameLikeIgnoreCaseAndDeleteFlg(String transactionName
																				, Short deleteFlg
																				, Pageable pageRequest);
}
