package com.csnth.ketoan.common.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.csnth.ketoan.common.entities.generate.Product;

public interface IProductRepository extends JpaRepository<Product, Long> {
	public Page<Product> findByProductNameLikeIgnoreCase(String productName, Pageable pageRequest);

	public Page<Product> findByProductNameLikeIgnoreCaseAndDeleteFlg(String productName
																	, Short deleteFlg
																	, Pageable pageRequest);
	public List<Product> findByDeleteFlg(Short deleteFlg);
}
