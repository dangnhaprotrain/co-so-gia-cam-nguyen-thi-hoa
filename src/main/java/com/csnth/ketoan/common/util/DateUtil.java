package com.csnth.ketoan.common.util;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtil {
	public static Timestamp getCurrentTime() {
		return new Timestamp(System.currentTimeMillis());
	}

	public static String formatDateYYYY(Date date) {
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		return dateFormat.format(date);
	}

	public static Date convertStringToDate(String typeDate, String date) {
		Date dateFormat = null;
		SimpleDateFormat formatter = new SimpleDateFormat(typeDate);
		try {
			dateFormat = formatter.parse(date);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return dateFormat;

	}
}
