package com.csnth.ketoan.common.config;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.csnth.ketoan.auth.entities.UserAuth;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Provides a basic implementation of the UserDetails interface
 */
public class CustomUserDetails implements UserDetails {

	private static final long serialVersionUID = 1L;
	private Collection<? extends GrantedAuthority> authorities;
	private String password;
	private String username;
	private Long userId;

	public CustomUserDetails(List<UserAuth> userAuthList) {
		this.username = userAuthList.get(0).getUsername();
		this.password = userAuthList.get(0).getPassword();
		this.userId = userAuthList.get(0).getUserId();
		this.authorities = translate(userAuthList);
	}

	/**
	 * Translates the List<Role> to a List<GrantedAuthority>
	 * 
	 * @param roles
	 *            the input list of roles.
	 * @return a list of granted authorities
	 */
	private Collection<? extends GrantedAuthority> translate(List<UserAuth> userAuthList) {
		List<GrantedAuthority> authorities = new ArrayList<>();
		if (CollectionUtils.isNotEmpty(userAuthList)) {
			for (UserAuth role : userAuthList) {
				String name = role.getRoleName().toUpperCase();
				// Make sure that all roles start with "ROLE_"
				if (!name.startsWith("ROLE_"))
					name = "ROLE_" + name;
				authorities.add(new SimpleGrantedAuthority(name));
			}
		}
		return authorities;
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return authorities;
	}

	@Override
	public String getPassword() {
		return password;
	}

	@Override
	public String getUsername() {
		return username;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}

}
