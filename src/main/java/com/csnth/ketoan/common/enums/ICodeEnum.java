package com.csnth.ketoan.common.enums;

public interface ICodeEnum {

	Short getCode();

	String getLabel();

	String getDescription();
}
