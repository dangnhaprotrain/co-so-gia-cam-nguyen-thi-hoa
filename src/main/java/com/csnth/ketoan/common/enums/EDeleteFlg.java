package com.csnth.ketoan.common.enums;

import java.util.HashMap;
import java.util.Map;

public enum EDeleteFlg implements ICodeEnum {
	DELETED((short)1, "deleted", "Deleted status"), ACTIVE((short)2, "Active", "Active status");

	private Short code;

	private String label;

	private String description;

	private static Map<Short, EDeleteFlg> codeMapping;

	private EDeleteFlg(Short code, String label, String description) {
		this.code = code;
		this.label = label;
		this.description = description;
	}

	public static EDeleteFlg getFormatType(Short i) {
		if (codeMapping == null) {
			initMapping();
		}
		return codeMapping.get(i);
	}

	public static Map<Short, EDeleteFlg> getCodeMapping() {
		if (codeMapping == null) {
			initMapping();
		}
		return codeMapping;
	}

	public static void initMapping() {
		codeMapping = new HashMap<Short, EDeleteFlg>();
		for (EDeleteFlg s : values()) {
			codeMapping.put(s.code, s);
		}
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder();
		sb.append(this.getClass().getName());
		sb.append("{code=").append(code);
		sb.append(", label='").append(label).append('\'');
		sb.append(", description='").append(description).append('\'');
		sb.append('}');
		return sb.toString();
	}

	@Override
	public Short getCode() {
		return code;
	}

	@Override
	public String getLabel() {
		return label;
	}

	@Override
	public String getDescription() {
		return description;
	}

}
