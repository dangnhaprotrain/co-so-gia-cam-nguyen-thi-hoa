package com.csnth.ketoan.common.enums;

import java.util.HashMap;
import java.util.Map;

public enum ETableAction implements ICodeEnum {
	EDIT((short) 1, "edit", "Edit action")
	, DELETE((short) 2, "delete", "Delete action")
	, RESTORE((short) 2, "restore", "Restore action");

	private Short code;

	private String label;

	private String description;

	private static Map<Short, ETableAction> codeMapping;

	private ETableAction(Short code, String label, String description) {
		this.code = code;
		this.label = label;
		this.description = description;
	}

	public static ETableAction getFormatType(Short i) {
		if (codeMapping == null) {
			initMapping();
		}
		return codeMapping.get(i);
	}

	public static Map<Short, ETableAction> getCodeMapping() {
		if (codeMapping == null) {
			initMapping();
		}
		return codeMapping;
	}

	public static void initMapping() {
		codeMapping = new HashMap<Short, ETableAction>();
		for (ETableAction s : values()) {
			codeMapping.put(s.code, s);
		}
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder();
		sb.append(this.getClass().getName());
		sb.append("{code=").append(code);
		sb.append(", label='").append(label).append('\'');
		sb.append(", description='").append(description).append('\'');
		sb.append('}');
		return sb.toString();
	}

	@Override
	public Short getCode() {
		return code;
	}

	@Override
	public String getLabel() {
		return label;
	}

	@Override
	public String getDescription() {
		return description;
	}

}
