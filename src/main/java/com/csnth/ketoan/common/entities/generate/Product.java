package com.csnth.ketoan.common.entities.generate;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.csnth.ketoan.common.constant.SequenceName;

/**
 * The persistent class for the company database table.
 * 
 */
@Entity
@Table(name = "product")
public class Product implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "product_seq_generate")
	@SequenceGenerator(name = "product_seq_generate", sequenceName = SequenceName.PRODUCT_SEQ, initialValue = 1, allocationSize = 1)
	private Long id;

	@Column(name = "product_name")
	private String productName;

	@Column(name = "delete_flg")
	private Short deleteFlg;

	public Product() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public Short getDeleteFlg() {
		return deleteFlg;
	}

	public void setDeleteFlg(Short deleteFlg) {
		this.deleteFlg = deleteFlg;
	}

}