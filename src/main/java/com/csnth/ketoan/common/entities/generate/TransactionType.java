package com.csnth.ketoan.common.entities.generate;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.csnth.ketoan.common.constant.SequenceName;

/**
 * The persistent class for the transaction_type database table.
 * 
 */
@Entity
@Table(name = "transaction_type")
public class TransactionType implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "transaction_type_seq_generate")
	@SequenceGenerator(name = "transaction_type_seq_generate", sequenceName = SequenceName.TRANSACTION_TYPE_SEQ, initialValue = 1, allocationSize = 1)
	private Long id;

	@Column(name = "product_id")
	private Long productId;

	@Column(name = "created_by")
	private Long createdBy;

	@Column(name = "created_date")
	private Timestamp createdDate;

	@Column(name = "transaction_name")
	private String transactionName;

	@Column(name = "updated_by")
	private Long updatedBy;

	@Column(name = "updated_date")
	private Timestamp updatedDate;

	@Column(name = "delete_flg")
	private Short deleteFlg;

	public TransactionType() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	public Timestamp getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}

	public String getTransactionName() {
		return this.transactionName;
	}

	public void setTransactionName(String transactionName) {
		this.transactionName = transactionName;
	}

	public Long getUpdatedBy() {
		return this.updatedBy;
	}

	public void setUpdatedBy(Long updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Timestamp getUpdatedDate() {
		return this.updatedDate;
	}

	public void setUpdatedDate(Timestamp updatedDate) {
		this.updatedDate = updatedDate;
	}

	public Short getDeleteFlg() {
		return deleteFlg;
	}

	public void setDeleteFlg(Short deleteFlg) {
		this.deleteFlg = deleteFlg;
	}

	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

}