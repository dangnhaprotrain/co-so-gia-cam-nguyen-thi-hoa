package com.csnth.ketoan.common.entities.generate;

import java.io.Serializable;
import javax.persistence.*;

import com.csnth.ketoan.common.constant.SequenceName;

import java.sql.Timestamp;

/**
 * The persistent class for the company database table.
 * 
 */
@Entity
public class Company implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "company_seq_generate")
	@SequenceGenerator(name = "company_seq_generate"
					, sequenceName = SequenceName.COMPANY_SEQ
					, initialValue = 1, allocationSize = 1)
	private Long id;

	@Column(name = "branch_id")
	private Long branchId;

	@Column(name = "company_name")
	private String companyName;

	@Column(name = "created_by")
	private Long createdBy;

	@Column(name = "created_date")
	private Timestamp createdDate;

	@Column(name = "delete_flg")
	private Short deleteFlg;

	@Column(name = "phone_number")
	private String phoneNumber;

	@Column(name = "updated_by")
	private Long updatedBy;

	@Column(name = "updated_date")
	private Timestamp updatedDate;

	public Company() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getBranchId() {
		return this.branchId;
	}

	public void setBranchId(Long branchId) {
		this.branchId = branchId;
	}

	public String getCompanyName() {
		return this.companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public Long getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	public Timestamp getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}

	public Short getDeleteFlg() {
		return this.deleteFlg;
	}

	public void setDeleteFlg(Short deleteFlg) {
		this.deleteFlg = deleteFlg;
	}

	public String getPhoneNumber() {
		return this.phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public Long getUpdatedBy() {
		return this.updatedBy;
	}

	public void setUpdatedBy(Long updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Timestamp getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Timestamp updatedDate) {
		this.updatedDate = updatedDate;
	}

}