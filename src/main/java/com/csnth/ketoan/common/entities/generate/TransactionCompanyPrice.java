package com.csnth.ketoan.common.entities.generate;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * The persistent class for the transaction_company_price database table.
 * 
 */
@Entity
@Table(name = "transaction_company_price")
public class TransactionCompanyPrice implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private Long id;

	@Column(name = "trade_relations_id")
	private Long tradeRelationsId;

	@Column(name = "created_by")
	private Long createdBy;

	@Column(name = "created_date")
	private Timestamp createdDate;

	@Column(name = "price_default")
	private BigDecimal priceDefault;

	private Integer status;

	@Column(name = "time_apply_from")
	private Timestamp timeApplyFrom;

	@Column(name = "time_apply_to")
	private Timestamp timeApplyTo;

	@Column(name = "transaction_type_id")
	private Long transactionTypeId;

	@Column(name = "updated_by")
	private Long updatedBy;

	@Column(name = "updated_date")
	private Timestamp updatedDate;

	public TransactionCompanyPrice() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getTradeRelationsId() {
		return tradeRelationsId;
	}

	public void setTradeRelationsId(Long tradeRelationsId) {
		this.tradeRelationsId = tradeRelationsId;
	}

	public Long getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	public Timestamp getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}

	public BigDecimal getPriceDefault() {
		return this.priceDefault;
	}

	public void setPriceDefault(BigDecimal priceDefault) {
		this.priceDefault = priceDefault;
	}

	public Integer getStatus() {
		return this.status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Timestamp getTimeApplyFrom() {
		return this.timeApplyFrom;
	}

	public void setTimeApplyFrom(Timestamp timeApplyFrom) {
		this.timeApplyFrom = timeApplyFrom;
	}

	public Timestamp getTimeApplyTo() {
		return this.timeApplyTo;
	}

	public void setTimeApplyTo(Timestamp timeApplyTo) {
		this.timeApplyTo = timeApplyTo;
	}

	public Long getTransactionTypeId() {
		return this.transactionTypeId;
	}

	public void setTransactionTypeId(Long transactionTypeId) {
		this.transactionTypeId = transactionTypeId;
	}

	public Long getUpdatedBy() {
		return this.updatedBy;
	}

	public void setUpdatedBy(Long updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Timestamp getUpdatedDate() {
		return this.updatedDate;
	}

	public void setUpdatedDate(Timestamp updatedDate) {
		this.updatedDate = updatedDate;
	}

}