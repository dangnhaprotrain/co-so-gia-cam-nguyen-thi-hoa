package com.csnth.ketoan.common.entities.generate;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.csnth.ketoan.common.constant.SequenceName;

/**
 * The persistent class for the company database table.
 * 
 */
@Entity
@Table(name = "trade_relations")
public class TradeRelations implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "trade_relations_seq_generate")
	@SequenceGenerator(name = "trade_relations_seq_generate", sequenceName = SequenceName.TRADE_RELATIONS_SEQ, initialValue = 1, allocationSize = 1)
	private Long id;

	@Column(name = "user_id")
	private Long userId;

	@Column(name = "company_id")
	private Long companyId;

	@Column(name = "delete_flg")
	private Short deleteFlg;

	public TradeRelations() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}

	public Short getDeleteFlg() {
		return deleteFlg;
	}

	public void setDeleteFlg(Short deleteFlg) {
		this.deleteFlg = deleteFlg;
	}

}