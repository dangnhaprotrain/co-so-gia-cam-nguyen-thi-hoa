package com.csnth.ketoan.common.entities.generate;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * The persistent class for the statement_accounts database table.
 * 
 */
@Entity
@Table(name = "statement_accounts")
@NamedQuery(name = "StatementAccount.findAll", query = "SELECT s FROM StatementAccount s")
public class StatementAccount implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private Long id;

	private BigDecimal amount;

	private Long buyer;

	@Column(name = "created_by")
	private Long createdBy;

	@Column(name = "created_date")
	private Timestamp createdDate;

	private String note;

	private Long seller;

	@Column(name = "total_price")
	private BigDecimal totalPrice;

	@Column(name = "transaction_price")
	private BigDecimal transactionPrice;

	@Column(name = "transaction_type_id")
	private Long transactionTypeId;

	@Column(name = "updated_by")
	private Long updatedBy;

	@Column(name = "updated_date")
	private Timestamp updatedDate;

	public StatementAccount() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public BigDecimal getAmount() {
		return this.amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public Long getBuyer() {
		return this.buyer;
	}

	public void setBuyer(Long buyer) {
		this.buyer = buyer;
	}

	public Long getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	public String getNote() {
		return this.note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public Long getSeller() {
		return this.seller;
	}

	public void setSeller(Long seller) {
		this.seller = seller;
	}

	public BigDecimal getTotalPrice() {
		return this.totalPrice;
	}

	public void setTotalPrice(BigDecimal totalPrice) {
		this.totalPrice = totalPrice;
	}

	public BigDecimal getTransactionPrice() {
		return this.transactionPrice;
	}

	public void setTransactionPrice(BigDecimal transactionPrice) {
		this.transactionPrice = transactionPrice;
	}

	public Long getTransactionTypeId() {
		return this.transactionTypeId;
	}

	public void setTransactionTypeId(Long transactionTypeId) {
		this.transactionTypeId = transactionTypeId;
	}

	public Long getUpdatedBy() {
		return this.updatedBy;
	}

	public void setUpdatedBy(Long updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Timestamp getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}

	public Timestamp getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Timestamp updatedDate) {
		this.updatedDate = updatedDate;
	}

}