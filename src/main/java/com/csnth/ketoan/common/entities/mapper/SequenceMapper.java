package com.csnth.ketoan.common.entities.mapper;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.ColumnResult;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.ConstructorResult;
import javax.persistence.NamedNativeQuery;

@Entity
@SqlResultSetMapping(name = "SeqMapper", 
		classes = @ConstructorResult(targetClass = SequenceMapper.class, columns = { @ColumnResult(name = "next_value", type = Long.class) }))
@NamedNativeQuery(name = "SeqSql"
				, query = "select nextval(:seq_name) as next_value "
				, resultSetMapping = "SeqMapper")
public class SequenceMapper implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "next_value")
	private Long nextValue;

	public SequenceMapper(Long nextValue) {
		super();
		this.nextValue = nextValue;
	}

	public Long getNextValue() {
		return nextValue;
	}

	public void setNextValue(Long nextValue) {
		this.nextValue = nextValue;
	}
	
}
