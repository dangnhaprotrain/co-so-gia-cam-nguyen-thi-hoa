package com.csnth.ketoan.common.service;

public interface ISequenceService extends BaseService {

	Long getSequenceNextValueBySeqName(String seqName);
}
