package com.csnth.ketoan.common.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.csnth.ketoan.common.repository.ISequenceRepository;
import com.csnth.ketoan.common.service.ISequenceService;

@Service
public class SequenceServiceImpl implements ISequenceService {

	@Autowired
	private ISequenceRepository sequenceRepository;

	@Override
	public Long getSequenceNextValueBySeqName(String seqName) {
		return sequenceRepository.getSeqNextVal(seqName).getNextValue();
	}

}
