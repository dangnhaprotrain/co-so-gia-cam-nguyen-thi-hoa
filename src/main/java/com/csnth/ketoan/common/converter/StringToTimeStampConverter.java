package com.csnth.ketoan.common.converter;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.apache.commons.lang3.StringUtils;

import com.csnth.ketoan.common.constant.Const;

public class StringToTimeStampConverter {
	@SuppressWarnings("rawtypes")
	public Object convert(Class type, Object value) {
		if (value == null) {
			return null;
		} else if (value instanceof String) {
			if (StringUtils.isNotBlank(value.toString())) {
				SimpleDateFormat sdf = new SimpleDateFormat(Const.DATE_TIME_FORMAT_DDMMYYYY);
				try {
					return sdf.parse(value.toString());
				} catch (ParseException e) {
					e.printStackTrace();
					return value.toString();
				}
			} else {
				return null;
			}
		} else {
			return value;
		}
	}
}
