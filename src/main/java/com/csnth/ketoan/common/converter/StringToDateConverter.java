package com.csnth.ketoan.common.converter;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.csnth.ketoan.common.constant.Const;
import org.apache.commons.beanutils.Converter;

public final class StringToDateConverter implements Converter {

	@SuppressWarnings("rawtypes")
	public Object convert(Class type, Object value) {

		if (value == null) {
			return null;
		} else if (value instanceof Timestamp) {
			return new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(value);
		} else if (value instanceof Date) {
			SimpleDateFormat sdf = new SimpleDateFormat(Const.DATE_FORMAT_YYYYMMDD);
			return sdf.format(value);
		} else {
			return value.toString();
		}
	}

}
