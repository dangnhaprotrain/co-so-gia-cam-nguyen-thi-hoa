package com.csnth.ketoan.common.constant;

public class Router {
	public static final String VIEW = "view";

	public static final String ROOT_URI = "/";

	public static final String ROOT_PAGE = "pages/";

	public interface AUTH {

		public interface URI {

			public static final String LOGIN = ROOT_URI + "login";

			public static final String LOGOUT = ROOT_URI + "logout";

		}

		public interface PAGE {

			public static final String LOGIN = ROOT_PAGE + "auth/login";

		}
	}

	public interface PROFILE {

		public static final String PROFILE_ROOT_URI = ROOT_URI + "profile/";

		public static final String PROFILE_ROOT_PAGE = ROOT_PAGE + "profile/";

		public interface URI {

			public static final String TRADE_RELATIONS = PROFILE_ROOT_URI + "trade-relations";

		}

		public interface PAGE {

			public static final String TRADE_RELATIONS = PROFILE_ROOT_PAGE + "trade-relations/" + VIEW;

		}
	}

	public interface REPORT {

		public static final String REPORT_ROOT_URI = ROOT_URI + "report/";

		public static final String REPORT_ROOT_PAGE = ROOT_PAGE + "report/";

		public interface URI {

			public static final String STATEMENT_ACCOUNTS_PRVOVIDER = REPORT_ROOT_URI + "sa-provider";

		}

		public interface PAGE {

			public static final String STATEMENT_ACCOUNTS_PRVOVIDER = REPORT_ROOT_PAGE + "sa-provider/" + VIEW;

		}
	}

	public interface ADMIN {

		public static final String ADMIN_ROOT_URI = ROOT_URI + "admin/";

		public static final String ADMIN_ROOT_PAGE = ROOT_PAGE + "admin/";

		public interface URI {

			public static final String COMPANY = ADMIN_ROOT_URI + "company";

			public static final String TRANSACTION_TYPE = ADMIN_ROOT_URI + "transaction-type";

			public static final String USER = ADMIN_ROOT_URI + "user";

			public static final String TRANSACTION_TYPE_PRICE = ADMIN_ROOT_URI + "transaction-type-price";

			public static final String PRODUCT = ADMIN_ROOT_URI + "product";

		}

		public interface PAGE {

			public static final String COMPANY = ADMIN_ROOT_PAGE + "company/" + VIEW;

			public static final String TRANSACTION_TYPE = ADMIN_ROOT_PAGE + "transaction-type/" + VIEW;

			public static final String USER = ADMIN_ROOT_PAGE + "user/" + VIEW;

			public static final String TRANSACTION_TYPE_PRICE = ADMIN_ROOT_PAGE + "transaction-type-price/" + VIEW;

			public static final String PRODUCT = ADMIN_ROOT_PAGE + "product/" + VIEW;;
		}
	}

}
