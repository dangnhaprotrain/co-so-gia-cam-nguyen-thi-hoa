package com.csnth.ketoan.common.constant;

public class Const {

	/** String date format dd/MM/yyyy */
	public static final String DATE_FORMAT_DDMMYYYY = "dd/MM/yyyy";

	/** String date format yyyy/MM/dd */
	public static final String DATE_FORMAT_YYYYMMDD = "yyyy/MM/dd";

	/** String date format yyyy/MM/dd HH:mm:ss */
	public static final String DATE_TIME_FORMAT_DDMMYYYY = "yyyy/MM/dd HH:mm:ss";
}
