package com.csnth.ketoan.common.constant;

public class CodeConst {
	/** Controller Status */
	public interface ctrlStatus {
		/** Normal */
		public static final String NORMAL = "000";
		/** Input error */
		public static final String WEB_ERR = "100";
		/** DB related error */
		public static final String BIZ_ERR = "200";
	}
}
