package com.csnth.ketoan.common.constant;

public class SequenceName {
	public static final String COMPANY_SEQ = "company_seq";
	public static final String TRANSACTION_TYPE_SEQ = "transaction_type_seq";
	public static final String TRADE_RELATIONS_SEQ = "trade_relations_seq";
	public static final String PRODUCT_SEQ = "product_seq";
}
