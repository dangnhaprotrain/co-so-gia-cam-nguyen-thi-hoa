package com.csnth.ketoan.common.controller;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import com.csnth.ketoan.common.dto.UserLogin;

public interface IBaseController {

	default UserLogin getUserLoginInfor() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		return (UserLogin) auth.getPrincipal();
	}
}
