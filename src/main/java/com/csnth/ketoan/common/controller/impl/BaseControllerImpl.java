package com.csnth.ketoan.common.controller.impl;

import com.csnth.ketoan.common.controller.IBaseController;

public abstract class BaseControllerImpl implements IBaseController {
	public static final String STATUS = "status";

	public static final String MESSAGE = "message";

	protected static final int DEFAULT_PAGE_NUMBER = 0;

	protected static final int DEFAULT_PAGE_SIZE = 10;
}
