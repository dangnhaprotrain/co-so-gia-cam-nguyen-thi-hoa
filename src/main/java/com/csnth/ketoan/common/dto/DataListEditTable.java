package com.csnth.ketoan.common.dto;

public class DataListEditTable {
	private String index;
	private String value;

	public DataListEditTable() {
	}

	public DataListEditTable(String index, String value) {
		super();
		this.index = index;
		this.value = value;
	}

	public String getIndex() {
		return index;
	}

	public void setIndex(String index) {
		this.index = index;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}
