package com.csnth.ketoan.common.dto;

import java.util.ArrayList;
import java.util.List;

public class BaseForm {

	private List<ErrorInfo> errInfo = new ArrayList<ErrorInfo>();

	public List<ErrorInfo> getErrInfo() {
		return errInfo;
	}

	public void setErrInfo(List<ErrorInfo> errInfo) {
		this.errInfo = errInfo;
	}

	public boolean hasErrors() {

		for (ErrorInfo e : errInfo) {
			if (ErrorInfo.LEVEL_ERR.equals(e.getLevel())) {
				return true;
			}
		}

		return false;
	}

	public boolean hasWarning() {

		for (ErrorInfo e : errInfo) {
			if (ErrorInfo.LEVEL_WARNING.equals(e.getLevel())) {
				return true;
			}
		}

		return false;
	}

}
