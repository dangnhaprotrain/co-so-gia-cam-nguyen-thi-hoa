package com.csnth.ketoan.auth.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import com.csnth.ketoan.common.constant.Router;

@Controller
public class AuthController {
	
	@GetMapping(value = { Router.ROOT_URI, Router.AUTH.URI.LOGIN })
	public String login(Model model, String error, String logout) {
		if (error != null) {
			model.addAttribute("error", "Xác thực tài khoản thất bại!");
		}
		if (logout != null) {
			model.addAttribute("message", "Đăng xuất thành công!.");
		}
		return Router.AUTH.PAGE.LOGIN;
	}

	@GetMapping(value = Router.AUTH.URI.LOGOUT)
	public String logoutPage(HttpServletRequest request, HttpServletResponse response) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (auth != null) {
			SecurityContextHolder.getContext().setAuthentication(null);
		}
		return "redirect:/login?logout";
	}
}
