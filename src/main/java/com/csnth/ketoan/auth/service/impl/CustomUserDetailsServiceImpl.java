package com.csnth.ketoan.auth.service.impl;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.csnth.ketoan.auth.entities.UserAuth;
import com.csnth.ketoan.auth.repository.impl.UserAuthRepositoryImpl;
import com.csnth.ketoan.common.dto.UserLogin;

@Service
public class CustomUserDetailsServiceImpl implements UserDetailsService {
	@Autowired
	private UserAuthRepositoryImpl userRepository;

	@Override
	@Transactional(readOnly = true)
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		List<UserAuth> user = userRepository.findUserAuthByUsername(username);
		if (CollectionUtils.isEmpty(user)) {
			throw new UsernameNotFoundException("Username " + username + " not found");
		}
		return new UserLogin(user.get(0).getUserId(), user.get(0).getUsername(), user.get(0).getPassword(),
				getGrantedAuthorities(user));
	}

	private Set<GrantedAuthority> getGrantedAuthorities(List<UserAuth> user) {
		Set<GrantedAuthority> grantedAuthorities = new HashSet<>();
		for (UserAuth u : user) {
			String name = u.getRoleName().toUpperCase();
			// Make sure that all roles start with "ROLE_"
			if (!name.startsWith("ROLE_"))
				name = "ROLE_" + name;
			grantedAuthorities.add(new SimpleGrantedAuthority(name));
		}
		return grantedAuthorities;

	}

}
