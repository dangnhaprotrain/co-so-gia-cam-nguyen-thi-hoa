package com.csnth.ketoan.auth.repository;

import java.util.List;

import com.csnth.ketoan.auth.entities.UserAuth;;

/**
 * User repository for CRUD operations.
 */
public interface UserAuthRepository{
    List<UserAuth> findUserAuthByUsername(String username);
}
