package com.csnth.ketoan.auth.repository.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import com.csnth.ketoan.auth.entities.UserAuth;
import com.csnth.ketoan.auth.repository.UserAuthRepository;

@Repository
public class UserAuthRepositoryImpl implements UserAuthRepository {
	
	@PersistenceContext
	public EntityManager em;
	
	@SuppressWarnings("unchecked")
	@Override
	public List<UserAuth> findUserAuthByUsername(String username) {
		return em.createNamedQuery("UserAuthSql").setParameter(1, username).getResultList();
	}

}
