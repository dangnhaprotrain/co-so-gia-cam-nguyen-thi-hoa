package com.csnth.ketoan.auth.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.ColumnResult;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.ConstructorResult;
import javax.persistence.NamedNativeQuery;

@Entity
@SqlResultSetMapping(name = "UserAuthMapper", 
classes = @ConstructorResult(targetClass = UserAuth.class, columns = {
			@ColumnResult(name = "user_id", type = Long.class), @ColumnResult(name = "password", type = String.class),
			@ColumnResult(name = "username", type = String.class), @ColumnResult(name = "role_id", type = Long.class),
			@ColumnResult(name = "role_name", type = String.class) }))
@NamedNativeQuery(name = "UserAuthSql"
				, query = "SELECT "
						+ " 	users.ID AS user_id "
						+ "		, users.PASSWORD "
						+ "		, users.username "
						+ "		, ROLE.ID AS role_id "
						+ "		, ROLE.role_name "
						+ "FROM "
						+ "		users "
						+ "INNER JOIN ROLE ON users. ID = ROLE.ID "
						+ "WHERE "
						+ "	users.username = ? "
				, resultSetMapping = "UserAuthMapper")
public class UserAuth implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	private Long id;
	
	@Column(name="user_id")
	private Long userId;

	private String password;

	private String username;
	
	@Column(name="role_id")
	private Long roleId;

	@Column(name="role_name")
	private String roleName;

	
	public UserAuth(Long userId, String password, String username, Long roleId, String roleName) {
		super();
		this.userId = userId;
		this.password = password;
		this.username = username;
		this.roleId = roleId;
		this.roleName = roleName;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public Long getRoleId() {
		return roleId;
	}

	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
	
	
}
