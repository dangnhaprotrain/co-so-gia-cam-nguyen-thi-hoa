package com.csnth.ketoan;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KeToanGaApplication {

	public static void main(String[] args) {
		SpringApplication.run(KeToanGaApplication.class, args);
	}
}
