package com.csnth.ketoan.profile.dto.dxo;

import com.csnth.ketoan.common.dto.DXO;

public class FindAllTradeRelationsDxoDto extends DXO {

	private static final long serialVersionUID = 1L;

	private String companyName;

	private int deletedFlg;

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public int getDeletedFlg() {
		return deletedFlg;
	}

	public void setDeletedFlg(int deletedFlg) {
		this.deletedFlg = deletedFlg;
	}

}
