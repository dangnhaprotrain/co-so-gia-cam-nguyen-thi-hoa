package com.csnth.ketoan.profile.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.csnth.ketoan.common.dto.DXO;
import com.csnth.ketoan.common.service.BaseService;
import com.csnth.ketoan.profile.entities.mapper.FindAllTradeRelationsMapper;

public interface ITradeRelationsService extends BaseService {
	Page<FindAllTradeRelationsMapper> findAll(Pageable pageable, DXO dxoDto);
}
