package com.csnth.ketoan.profile.service.impl;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.csnth.ketoan.admin.dto.dxo.FindAllCompanyDxoDto;
import com.csnth.ketoan.admin.entities.mapper.FindAllCompanyMapper;
import com.csnth.ketoan.common.dto.DXO;
import com.csnth.ketoan.profile.dto.dxo.FindAllTradeRelationsDxoDto;
import com.csnth.ketoan.profile.entities.mapper.FindAllTradeRelationsMapper;
import com.csnth.ketoan.profile.repository.IFindAllTradeRelationsRepository;
import com.csnth.ketoan.profile.service.ITradeRelationsService;

@Service
public class TradeRelationsServiceImpl implements ITradeRelationsService {

	@Autowired
	private IFindAllTradeRelationsRepository findAllTradeRelationsRepository;

	@Override
	public Page<FindAllTradeRelationsMapper> findAll(Pageable pageable, DXO dxoDto) {
		FindAllTradeRelationsDxoDto dxo = (FindAllTradeRelationsDxoDto) dxoDto;
		//String companyNameSearch = StringUtils.trim(StringUtils.defaultIfEmpty(dxo.getCompanyName(), StringUtils.EMPTY)).toLowerCase();
		Page<FindAllTradeRelationsMapper> pageData = findAllTradeRelationsRepository
				.findAllTradeRelations("%".concat("").concat("%")
								, 1
								, pageable);
		return pageData;
	}

}
