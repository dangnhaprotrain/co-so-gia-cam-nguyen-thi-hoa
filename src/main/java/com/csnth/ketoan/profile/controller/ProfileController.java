package com.csnth.ketoan.profile.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.data.web.SortDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;

import com.csnth.ketoan.common.constant.Router;
import com.csnth.ketoan.common.controller.impl.BaseControllerImpl;
import com.csnth.ketoan.common.util.BeanUtil;
import com.csnth.ketoan.profile.dto.dxo.FindAllTradeRelationsDxoDto;
import com.csnth.ketoan.profile.dto.form.TradeRelationsSearchForm;
import com.csnth.ketoan.profile.service.ITradeRelationsService;

@Controller
public class ProfileController extends BaseControllerImpl {

	@Autowired
	private ITradeRelationsService tradeRelationsService;

	@GetMapping(value = Router.PROFILE.URI.TRADE_RELATIONS)
	public String view(@ModelAttribute("searchForm") TradeRelationsSearchForm searchForm
						, Model model
						, @PageableDefault(size = DEFAULT_PAGE_SIZE) 
						@SortDefault.SortDefaults({@SortDefault(sort = "id", direction = Sort.Direction.ASC) }) 
						Pageable pageable) {
		/*FindAllTradeRelationsDxoDto dxo = (FindAllTradeRelationsDxoDto) BeanUtil
															.createAndCopyPropertiesNative(searchForm
																						, FindAllTradeRelationsDxoDto.class);*/
//		model.addAttribute("pageList", companyService.findAllCompany(pageable, dxo));
		return Router.PROFILE.PAGE.TRADE_RELATIONS;
	}
}
