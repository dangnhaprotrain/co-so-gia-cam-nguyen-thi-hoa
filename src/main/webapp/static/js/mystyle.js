/* menu hover */
$(document).ready(function(){
    checkSize();
    boxToggle();
    slideToggle();
    slideToggleDouble();
    changeText();
    updateAll();
    deleteAll();
    modalDeleteShow();
    modalApprovalShow();
    messageSuccess();
    messageApprovalSuccess();
    slideDown();
    slideShow();
    messageConfirmSuccess();
    searchChangeDate();
    checkDay();
    slideShowRestaurant();
    backModalfromDelete();
    backModalfromApproval();
    calendar();
    modalConfirmOrderShow();
    boxPlateShow();
});

function boxPlateShow(){
    $(".box-show-plate").hide();
    $(".check-plate").click(function() {
        if($(this).is(":checked")) {
            $(".box-show-plate").slideDown();
        } else {
            $(".box-show-plate").slideUp();
        }
    });
}

function modalConfirmOrderShow(){
    $('.select-confirm-adress').on('changed.bs.select', function() {

      $(".modal-confirm-order-adress").modal('show');
      
    });
    $('.select-confirm-office').on('changed.bs.select', function() {
      $(".modal-confirm-order-office").modal('show');
      
    });

}

function modalApprovalShow(){
    $('.btn-detail-approval').click(function(e){
        e.preventDefault();

        $('#modal-detail')
            .modal('hide')
            .on('hidden.bs.modal', function (e) {
                $('#modal-approval').modal('show');

                $(this).off('hidden.bs.modal'); // Remove the 'on' event binding
            });
    });
}

function messageApprovalSuccess(){
    $(".btn-confirm-approval").click(function(){

         $('#modal-approval')
            .modal('hide')
            .on('hidden.bs.modal', function (e) {
                $('#modal-approval-success').modal({backdrop: "static"});

                $(this).off('hidden.bs.modal'); // Remove the 'on' event binding
            });

     
        $("#modal-approval-success").on('shown.bs.modal', function () {
            setTimeout(function() { $('#modal-approval-success').modal("hide"); }, 3000);
        });
    });
}
function modalDeleteShow(){
    $('.btn-detail-delete').click(function(e){
        e.preventDefault();

        $('#modal-detail')
            .modal('hide')
            .on('hidden.bs.modal', function (e) {
                $('#modal-delete').modal('show');

                $(this).off('hidden.bs.modal'); // Remove the 'on' event binding
            });
    });
}
function backModalfromDelete(){
    $('.btn-back-modal').click(function(e){
        e.preventDefault();

        $('#modal-delete')
            .modal('hide')
            .on('hidden.bs.modal', function (e) {
                $('#modal-detail').modal('show');

                $(this).off('hidden.bs.modal'); // Remove the 'on' event binding
            });
    });
}

function backModalfromApproval(){
    $('.btn-back-modal-2').click(function(e){
        e.preventDefault();

        $('#modal-approval')
            .modal('hide')
            .on('hidden.bs.modal', function (e) {
                $('#modal-detail').modal('show');

                $(this).off('hidden.bs.modal'); // Remove the 'on' event binding
            });
    });
}

function messageSuccess(){
    $(".btn-confirm").click(function(){
         $('.modal-confirm')
            .modal('hide')
            .on('hidden.bs.modal', function (e) {
                $('.modal-success').modal({backdrop: "static"});

                $(this).off('hidden.bs.modal'); // Remove the 'on' event binding
            });

        /*$(".modal-success").modal("show");*/
        $(".modal-success").on('shown.bs.modal', function () {
            setTimeout(function() { $('.modal-success').modal("hide"); }, 3000);
        });
    });
}

function updateAll(){
    $(".cb-update-all").change( function(){
       if($(this).is(':checked')){
            $(".cb-update").prop("checked", true);
            
            if($(".cb-delete-all").is(':checked') ){
                $(".cb-delete-all").prop("checked", false);
            }

            $(".cb-delete").prop("checked", false);
        }else{
            $(".cb-update").prop("checked", false);

            if(!$(".cb-delete-all").is(':checked')){
                $(".cb-delete-all").prop("checked", true);
            }

            $(".cb-delete").prop("checked", true);
        }
    });
    $(".cb-update-all").change( function(){
        if(!$(this).is(':checked')){
            $(this).parents(".check-table").find(".cb-delete-all").prop("checked", false);
            $(this).parents(".check-table").find(".cb-delete").prop("checked", false);
        }
    });

    $(".cb-update").change( function(){
        if(!$(this).is(':checked')){
            $(".cb-update-all").prop("checked", false);
        }
    });
    $(".cb-update").change( function(){
        if($(this).is(':checked')){
            $(this).parents(".checkparents").find(".cb-delete").prop("checked", false);
            $(this).parents(".check-table").find(".cb-delete-all").prop("checked", false);
        }
    });
}

function deleteAll(){
    $(".cb-delete-all").change( function(){
       if($(this).is(':checked')){
            $(".cb-delete").prop("checked", true);
            
            if($(".cb-update-all").is(':checked')){
                $(".cb-update-all").prop("checked", false);
            }

            $(".cb-update").prop("checked", false);
        }else{
            $(".cb-delete").prop("checked", false);

            if(!$(".cb-delete-all").is(':checked')){
                $(".cb-update-all").prop("checked", true);
            }

            $(".cb-update").prop("checked", true);
        }
    });

    $(".cb-delete-all").change( function(){
        if(!$(this).is(':checked')){
            $(this).parents(".check-table").find(".cb-update-all").prop("checked", false);
            $(this).parents(".check-table").find(".cb-update").prop("checked", false);
        }
    });

    $(".cb-delete").change( function(){
        if(!$(this).is(':checked')){
            $(".cb-delete-all").prop("checked", false);
        }
    });
    $(".cb-delete").change( function(){
        if($(this).is(':checked')){
            $(this).parents(".checkparents").find(".cb-update").prop("checked", false);
            $(this).parents(".check-table").find(".cb-update-all").prop("checked", false);
        }
    });
}

function boxToggle(){
    $(".box-toggle").hide();
    $(".box-toggle-2").hide();

    $(".btn-box-toggle").click( function(){
       $(".box-toggle").slideToggle();
       $(".btn-box-toggle .fa").toggleClass('fa-chevron-circle-down');
       setTimeout(function() {
            $("input").first().not(".box-calendar input").focus();
       }, 500);
    });

    $(".btn-box-toggle-2").click( function(){
       $(".box-toggle-2").slideToggle();
       $(".btn-box-toggle-2 .fa").toggleClass('fa-chevron-circle-down');
    });
}

function slideToggle(){
    $(".slide-toggle").hide();

    if($("#slide-down").is(':checked')){
        $(".slide-toggle").show();
    }

    $("#slide-down").change( function(){
       if($(this).is(':checked')){
            $(".slide-toggle").slideDown();
        }
    });

    $("#slide-up").change( function(){
       if($(this).is(':checked')){
            $(".slide-toggle").slideUp();
        }
    });

    $(".btn-slide-toggle").click(function(){
        $(".slide-toggle").slideToggle();
    });

    $(".slide-toggle-menu-mutil").hide();

    if($(".radio-menu-multi").is(':checked')){
        $(".slide-toggle-menu-mutil").show();
    }

    $(".radio-menu-multi").change( function(){
       if($(this).is(':checked')){
            $(".slide-toggle-menu-mutil").slideDown();
        }
    });

    $(".radio-menu-single").change( function(){
       if($(this).is(':checked')){
            $(".slide-toggle-menu-mutil").slideUp();
        }
    });  
}

function slideToggleDouble(){
    $(".slide-toggle-1").hide();
    $(".slide-toggle-2").hide();

    if($("#slide-1").is(':checked')){
        $(".slide-toggle-1").show();
    }

    $("#slide-1").change( function(){
       if($(this).is(':checked')){
            $(".slide-toggle-1").slideDown();
            $(".slide-toggle-2").slideUp();
        }
    });

    if($("#slide-2").is(':checked')){
        $(".slide-toggle-2").show();
    }

    $("#slide-2").change( function(){
       if($(this).is(':checked')){
            $(".slide-toggle-1").slideUp();
            $(".slide-toggle-2").slideDown();
        }
    });
}

function changeText(){
    if($("#radio-change-1").is(':checked')){
        $(".txt-change").text('補助比率(％) *2');
        $(".txt-change-2").attr("placeholder", "(最大50)").val("").focus().blur();
    }

    $("#radio-change-1").change( function(){
       if($(this).is(':checked')){
            $(".txt-change").text('補助比率(％) *2');
            $(".txt-change-2").attr("placeholder", "(最大50)").val("").focus().blur();
        }
    });

    $("#radio-change-2").change( function(){
       if($(this).is(':checked')){
             $(".txt-change").text('補助額(税込) *2');
             $(".txt-change-2").attr("placeholder", "(税込)").val("").focus().blur();
        }
    });
}

function checkSize(){
    if($(window).width() <= 1200){
        $(".header-2 .navbar-main li").click(function(){
            $(this).children(".sub-menu").slideToggle(300);
        });
    }
    else{
        
    } 
}
function makeValidateDateMessage(object, message)
{
    var messageError = $(object).parent().find("p.message-error");
    if(messageError.length == 0) {
        $(object).parent().append('<p class="message-error semi-hidden"></p>');
    }
    $(object).keydown(function () {
        $(object).removeClass("is-not-valid");
        $(object).parent().find("p.message-error").css('display', 'none');
    });
    $(object).change(function () {
        $(object).removeClass("is-not-valid");
        $(object).parent().find("p.message-error").css('display', 'none');
    });
    $(object).addClass("is-not-valid");
    $(object).parent().find("p.message-error").html("日付が不正です。");
    $(object).parent().find("p.message-error").css('display', 'block');
}

function hideRedundantWeek() {
    var rows = $('.datepicker .datepicker-days table.table-condensed tbody tr');

    var rightDays = false;
    var nextDays = false;
    var donePrev = false;
    var reachNext = false;

    for(var i = 0; i < rows.length; i++) {
        var days = $(rows[i]).find('td');
        for(var j = 0; j < days.length && !donePrev; j++) {
            if(!donePrev) {
                if ($(days[j]).html() == '1') {
                    donePrev = true;
                }
            }
        }

        if(donePrev && rightDays && !reachNext) {
            for(j = 0; j < days.length && !reachNext; j++) {
                if(!reachNext) {
                    if ($(days[j]).html() == '1') {
                        reachNext = true;
                    }
                }
            }
        }

        if(nextDays) {
            $(rows[i]).find('td').css('display', 'none');
        }

        if(reachNext) {
            nextDays = true;
        }

        if(donePrev) {
            rightDays = true;
        }

        if(!donePrev) {
            $(rows[i]).find('td').css('display', 'none');
        }

    }
}

function calendar(){

    $('.bootstrap-calendar-1').datepicker({
        language: "ja",
        orientation: "bottom auto",
        format: "yyyy年mm月dd日(D)",
        autoclose: true,
        todayHighlight: true,
        forceParse: false
    })

    $('.bootstrap-calendar-1').each(function() {
        $(this).blur(function() {
            var datepicker = $('.datepicker');

            if(datepicker.length <= 0) {
                var temp = $(this).val();

                var dPGlobal = $(this).datepicker.DPGlobal;

                var parseDate = dPGlobal.formatDate(dPGlobal.parseDate(temp, "yyyy年mm月dd日(D)", "ja"), "yyyy年mm月dd日(D)", "ja");

                var tempSubBefore = '';
                var tempSubAfter = '';
                var parseDateSubBefore = '';
                var parseDateSubAfter = '';

                if(temp.indexOf('月') > 0) {
                    tempSubBefore = temp.substring(0, temp.indexOf('月') + 1);
                    tempSubAfter = temp.substring(temp.indexOf('月') + 1);
                    parseDateSubBefore = parseDate.substring(0, parseDate.indexOf('月') + 1);
                    parseDateSubAfter = parseDate.substring(parseDate.indexOf('月') + 1);
                    if(tempSubAfter.indexOf('(') >= 0) {
                        tempSubAfter = tempSubAfter.substring(0, tempSubAfter.indexOf('('));
                    }

                    if(tempSubBefore == parseDateSubBefore && parseDateSubAfter.indexOf(tempSubAfter) >= 0 && temp != '') {
                        $(this).val(parseDate);
                        $(this).datepicker("update");
                    } else {
                        $(this).val('');
                        $(this).datepicker("update");
                    }
                } else {
                        $(this).val('');
                        $(this).datepicker("update");
                }
            }
    
        });
        $(this).change(function() {
            hideRedundantWeek();
        });

        $(this).click(function() {
            if($(this).is(":focus")) {
                var temp = $(this).val();
                var highLighted = $('.datepicker .datepicker-days table.table-condensed tbody tr tdd.active.day');
                if (temp != '' && highLighted.length == 0) {
                    var dPGlobal = $(this).datepicker.DPGlobal;

                    var parseDate = dPGlobal.formatDate(dPGlobal.parseDate(temp, "yyyy年mm月dd日(D)", "ja"), "yyyy年mm月dd日(D)", "ja");

                    var tempSubBefore = '';
                    var tempSubAfter = '';
                    var parseDateSubBefore = '';
                    var parseDateSubAfter = '';

                    if(temp.indexOf('月') > 0) {
                        tempSubBefore = temp.substring(0, temp.indexOf('月') + 1);
                        tempSubAfter = temp.substring(temp.indexOf('月') + 1);
                        parseDateSubBefore = parseDate.substring(0, parseDate.indexOf('月') + 1);
                        parseDateSubAfter = parseDate.substring(parseDate.indexOf('月') + 1);
                        if(tempSubAfter.indexOf('(') >= 0) {
                            tempSubAfter = tempSubAfter.substring(0, tempSubAfter.indexOf('('));
                        }

                        if(tempSubBefore == parseDateSubBefore && parseDateSubAfter.indexOf(tempSubAfter) >= 0 && temp != '') {
                            $(this).val(parseDate);
                            $(this).datepicker("update");
                        } else {
                            $(this).val('');
                            $(this).datepicker("update");
                        }
                    } else {
                            $(this).val('');
                            $(this).datepicker("update");
                    }
                }
            }
        });
    });


/*
    $('.bootstrap-calendar-1').datepicker()
        .on('show', function() {
            hideRedundantWeek();
        });*/

    $('.bootstrap-calendar-2').datepicker({
        language: "ja",
        orientation: "bottom auto",
        format: "yyyy年mm月",
        startView: 1,
        minViewMode: 1,
        maxViewMode: 2,
        autoclose: true,
        todayHighlight: true,
        forceParse: false
    });

    $('.bootstrap-calendar-2').each(function() {


       
        $(this).blur(function() {
            var datepicker = $('.datepicker');

            if(datepicker.length <= 0) {
                var temp = $(this).val();

                var dPGlobal = $(this).datepicker.DPGlobal;

                var parseDate = dPGlobal.formatDate(dPGlobal.parseDate(temp, "yyyy年mm月", "ja"), "yyyy年mm月", "ja");

                var tempSubBefore = '';
                var tempSubAfter = '';
                var parseDateSubBefore = '';
                var parseDateSubAfter = '';

                if(temp.indexOf('年') > 0) {
                    tempSubBefore = temp.substring(0, temp.indexOf('年') + 1);
                    tempSubAfter = temp.substring(temp.indexOf('年') + 1);
                    parseDateSubBefore = parseDate.substring(0, parseDate.indexOf('年') + 1);
                    parseDateSubAfter = parseDate.substring(parseDate.indexOf('年') + 1);

                    if(tempSubBefore == parseDateSubBefore && parseDateSubAfter.indexOf(tempSubAfter) >= 0 && temp != '') {
                        $(this).val(parseDate);
                        $(this).datepicker("update");
                    } else {
                        $(this).val('');
                        $(this).datepicker("update");
                    }
                } else {
                    $(this).val('');
                    $(this).datepicker("update");
                }

            
            }
        });
        $(this).change(function() {
            hideRedundantWeek();

            var yearcur = new Date().getYear() + 1900;
            var yearcurentpicker = $('.datepicker .datepicker-months table.table-condensed thead tr th.datepicker-switch').text();
            yearcurentpicker = yearcurentpicker.substring(0, yearcurentpicker.length - 1);

            if(yearcur == yearcurentpicker) {
                var monthcur = new Date().getMonth() + 1;
                $('.datepicker .datepicker-months table.table-condensed tbody tr td .month').each(function(){
                    var text = $(this).text();
                    text = text.substring(0, text.length - 1);
                    if(text == monthcur) {
                        $(this).css({"background":"#ccc","color":"#000"});
                        
                    }
                });
            }
            

        });
        $(this).click(function() {

            if($(this).is(":focus")) {
                var temp = $(this).val();
                var highLighted = $('.datepicker .datepicker-days table.table-condensed tbody tr tdd.active.day');
                if (temp != '' && highLighted.length == 0) {

                    var dPGlobal = $(this).datepicker.DPGlobal;

                    var parseDate = dPGlobal.formatDate(dPGlobal.parseDate(temp, "yyyy年mm月", "ja"), "yyyy年mm月", "ja");

                    var tempSubBefore = '';
                    var tempSubAfter = '';
                    var parseDateSubBefore = '';
                    var parseDateSubAfter = '';

                    if(temp.indexOf('年') > 0) {
                        tempSubBefore = temp.substring(0, temp.indexOf('年') + 1);
                        tempSubAfter = temp.substring(temp.indexOf('年') + 1);
                        parseDateSubBefore = parseDate.substring(0, parseDate.indexOf('年') + 1);
                        parseDateSubAfter = parseDate.substring(parseDate.indexOf('年') + 1);

                        if(tempSubBefore == parseDateSubBefore && parseDateSubAfter.indexOf(tempSubAfter) >= 0 && temp != '') {
                            $(this).val(parseDate);
                            $(this).datepicker("update");
                        } else {
                            $(this).val('');
                            $(this).datepicker("update");
                        }
                    } else {
                        $(this).val('');
                        $(this).datepicker("update");
                    }
                }
            }
        });

        $(this).on('show', function(){
            var yearcur = new Date().getYear() + 1900;
            var yearcurentpicker = $('.datepicker .datepicker-months table.table-condensed thead tr th.datepicker-switch').text();
            yearcurentpicker = yearcurentpicker.substring(0, yearcurentpicker.length - 1);

            if(yearcur == yearcurentpicker) {
                var monthcur = new Date().getMonth() + 1;
                $('.datepicker .datepicker-months table.table-condensed tbody tr td .month').each(function(){
                    var text = $(this).text();
                    text = text.substring(0, text.length - 1);
                    if(text == monthcur) {
                        $(this).css({"background":"#ccc","border-color":"#204d74","color":"#000"});
                        // $(this).hover(function(){
                        //     $(this).css({"background":"#ffc966","color":"#000"});
                        //     }, function(){
                        //     $(this).css({"background":"#ccc","color":"#000"});
                        // });
                    }
                });
            }
        });
           
        
    });

   

    $('.bootstrap-calendar-3').datepicker({
        language: "ja",
        orientation: "bottom auto",
        format: "mm月",
        startView: 1,
        minViewMode: 1,
        maxViewMode: 1,
        autoclose: true,
        todayHighlight: true,
        forceParse: false
    });

    $('.bootstrap-calendar-3').each(function() {
       
        $(this).blur(function() {
            var datepicker = $('.datepicker');

            if(datepicker.length <= 0) {
                var temp = $(this).val();

                var dPGlobal = $(this).datepicker.DPGlobal;

                var parseDate = dPGlobal.formatDate(dPGlobal.parseDate(temp, "mm月", "ja"), "mm月", "ja");

                if(parseDate.indexOf(temp) >= 0 && temp != '') {
                    $(this).val(parseDate);
                    $(this).datepicker("update");
                } else {
                    $(this).val('');
                    $(this).datepicker("update");
                }
            }
        });
        $(this).change(function() {
            hideRedundantWeek();
        });
        $(this).click(function() {
            if($(this).is(":focus")) {
                var temp = $(this).val();
                var highLighted = $('.datepicker .datepicker-days table.table-condensed tbody tr tdd.active.day');
                if (temp != '' && highLighted.length == 0) {
                    var dPGlobal = $(this).datepicker.DPGlobal;

                    var parseDate = dPGlobal.formatDate(dPGlobal.parseDate(temp, "mm月", "ja"), "mm月", "ja");

                    if(parseDate.indexOf(temp) >= 0 && temp != '') {
                        $(this).val(parseDate);
                        $(this).datepicker("update");
                    } else {
                        $(this).val('');
                        $(this).datepicker("update");
                    }
                }
            }
        });
    });

/*    $('.bootstrap-calendar-3').datepicker()
        .on('show', function() {
            hideRedundantWeek();
        });*/

$('.form-current-date').datepicker('setDate', 'now');
}



function slideDown(){
    $(".box-toggle").hide();

    $(".btn-slide-down").click(function(){
        $(".box-toggle").slideDown();
    });
}

function slideShow(){
    $(".slide-show").hide();

    $(".btn-slide-show-box").click(function(){
        $(".slide-show").slideDown();
        $(this).parent().parent().css("background","#f5f5c7");
    });

}
function messageConfirmSuccess(){
    $(".btn-confirm-success").click(function(){

         $('#modal-approval')
            .modal('hide')
            .on('hidden.bs.modal', function (e) {
                $('.modal-success').modal({backdrop: "static"});

                $(this).off('hidden.bs.modal'); // Remove the 'on' event binding
            });

        /*$(".modal-success").modal("show");*/
        $(".modal-success").on('shown.bs.modal', function () {
            setTimeout(function() { $('.modal-success').modal("hide"); }, 3000);
        });
    });
}
function searchChangeDate(){
    $(".searchDate1").hide();
    $(".searchDate2").hide();

    if($("#radio-search1").is(':checked')){
        $(".searchDate1").show();
        $(".searchDate2").hide();
        $(".customcol2").addClass("customw100p");
        $(".customradio").addClass("col-md-6 col-lg-40p");
        $(".customcol1").addClass("custom-mt");
    }

    $("#radio-search1").change( function(){
       if($(this).is(':checked')){
            $(".searchDate1").show();
            $(".searchDate2").hide();
            $(".customcol2").addClass("customw100p");
            $(".customradio").addClass("col-md-6 col-lg-40p");
            $(".customcol1").addClass("custom-mt");
        }
    });

    if($("#radio-search2").is(':checked')){
        $(".searchDate2").show();
        $(".searchDate1").hide();
        $(".customcol2").removeClass("customw100p");
        $(".customradio").removeClass("col-md-6 col-lg-40p");
        $(".customcol1").removeClass("custom-mt");
    }

    $("#radio-search2").change( function(){
       if($(this).is(':checked')){
            $(".searchDate2").show();
            $(".searchDate1").hide();
            $(".customcol2").removeClass("customw100p");
            $(".customradio").removeClass("col-md-6 col-lg-40p");
            $(".customcol1").removeClass("custom-mt");
        }
    });
}
function checkDay() {
    var checkboxes;
    $('#checkbox-sun').click(function () {
        checkboxes = $("#calendar .checkbox:nth-child(7n - 6)").find('input:checkbox');
        checkboxes.prop('checked', this.checked);
        checkboxes.click(function () {
            if (!this.checked) {
                $('#checkbox-sun').prop('checked', false);
            }
        })
    });
    $('#checkbox-mon').click(function () {
        var checkboxes = $("#calendar .checkbox:nth-child(7n - 5)").find('input:checkbox');
        checkboxes.prop('checked', this.checked);
        checkboxes.click(function () {
            if (!this.checked) {
                $('#checkbox-mon').prop('checked', false);
            }
        })
    });
    $('#checkbox-tue').click(function () {
        var checkboxes = $("#calendar .checkbox:nth-child(7n - 4)").find('input:checkbox');
        checkboxes.prop('checked', this.checked);
        checkboxes.click(function () {
            if (!this.checked) {
                $('#checkbox-tue').prop('checked', false);
            }
        })
    });
    $('#checkbox-wes').click(function () {
        var checkboxes = $("#calendar .checkbox:nth-child(7n - 3)").find('input:checkbox');
        checkboxes.prop('checked', this.checked);
        checkboxes.click(function () {
            if (!this.checked) {
                $('#checkbox-wes').prop('checked', false);
            }
        })
    });
    $('#checkbox-thu').click(function () {
        var checkboxes = $("#calendar .checkbox:nth-child(7n - 2)").find('input:checkbox');
        checkboxes.prop('checked', this.checked);
        checkboxes.click(function () {
            if (!this.checked) {
                $('#checkbox-thu').prop('checked', false);
            }
        })
    });
    $('#checkbox-fri').click(function () {
        var checkboxes = $("#calendar .checkbox:nth-child(7n - 1)").find('input:checkbox');
        checkboxes.prop('checked', this.checked);
        checkboxes.click(function () {
            if (!this.checked) {
                $('#checkbox-fri').prop('checked', false);
            }
        })
    });
    $('#checkbox-sat').click(function () {
        var checkboxes = $("#calendar .checkbox:nth-child(7n)").find('input:checkbox');
        checkboxes.prop('checked', this.checked);
        checkboxes.click(function () {
            if (!this.checked) {
                $('#checkbox-sat').prop('checked', false);
            }
        })
    });
}
function slideShowRestaurant(){
    $(".box-search-day-restaurant").hide();
    $(".box-search-restaurant").hide();

    $(".btn-box-search-day-restaurant").click(function(){
        $(".box-search-day-restaurant").slideToggle();
        $(".box-search-restaurant").slideUp();
    });
    $(".btn-box-search-restaurant").click(function(){
        $(".box-search-restaurant").slideToggle();
        $(".box-search-day-restaurant").slideUp();
    });


    $(".box-toggle-menu-restaurant").hide();

    $(".btn-search-restaurant").click(function(){
        $(".box-toggle-restaurant").slideDown();
        $(".box-toggle-menu-restaurant").slideUp();
    });

    $(".box-click-restaurant").click(function(){
        $(".box-toggle-menu-restaurant").slideDown();
        $(".box-toggle-restaurant").slideUp();
    });
    
}

